#!/bin/ash

echo "Configuring startup, setting installation to skip wizard and to run on ${PUBLIC_URI}, internal port: ${PORT}."
/upsource/bin/upsource.sh configure --listen-address=0.0.0.0 --base-url=${PUBLIC_URI} --listen-port=${PORT} -J-Ddisable.configuration.wizard.on.clean.install=true

echo "Starting Jetbrains Upsource."
/upsource/bin/upsource.sh run --no-browser