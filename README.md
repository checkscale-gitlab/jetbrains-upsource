# Jetbrains Upsource docker image.

Docker image for jetbrains Upsource using the zip version.
The image is based on **Alpine linux** using **OpenJDK**.

_php7 and python packages added to enable Upsource to work with those languages._

Image can be found at [GitLab](https://gitlab.com/jitesoft/dockerfiles/jetbrains-upsource).

## Directories worth persisting

The following directories are worth persisting if you wish to store the data even when restarting the 
container:

`/upsource/data` - The data directory that is used by Upsource.  
`/upsource/backups` - Backup directory which backups are stored in.  
`/upsource/logs` - Logfiles.  
`/upsource/conf` - Config directory.  

The directories are set as `VOLUMES` in the docker file.

## Environment variables

Port 8080 is the default port used by Upsource, if you wish to use another port for some reason, 
change the env variable `PORT` to desired value and expose it.

Due to the upsource frontend calling itself by using a base-uri variable in the configuration,
the `BASE_URI` env variable is exposed. Set it to your public url (including port and schema (`http:// or https://`)) 
for it to work as expected. The variable defaults to `http://127.0.0.1:8080`.


## Initial startup

When starting Upsource for the first time, the installation will run automatically, you can go to `http://127.0.0.1:8080/starting`
to see the progress. When all is ready, the default administration account have the credentials `admin`/`admin`, I recommend
that you change this as soon as possible!

## External or Internal hub?

After initial startup your Upsource will use an internal Jetbrains Hub, to migrate to an external hub installation
you have to acquire the AWC token from `/upsource/data/internal/services/adminService/awcToken.properties`, easiest way
is to cat it from the container:

```bash
docker exec <your-container> cat /upsource/data/internal/services/adminService/awcToken.properties
```

Then enter the Bundle admin at `http://127.0.0.1:8080/bundle/admin/` and enter the token in the field and follow instructions.

## Tags

Tags are defined by the following standard:

`latest` latest build.  
`year` latest of the `year` builds (i.e., 2018).  
`year.x` latest of the `year-major` builds (i.e., 2018.4).  
`yearh.x.x` latest of the `year.major.minor` builds (i.e., 2018.4.1)
