FROM openjdk:8-jre-alpine
LABEL maintainer="Johannes Tegnér <johannes@jitesoft.com>"

ARG VERSION="2018.2.1154"
ENV PORT="8080" \
    PUBLIC_URI="http://127.0.0.1:8080"

ADD startup.sh /startup.sh
ADD healthcheck.sh /healthcheck.sh

RUN apk add --no-cache --virtual .trash curl unzip && \
    apk add --no-cache php7 python && \
    curl -OLsS https://download.jetbrains.com/upsource/upsource-${VERSION}.zip && \
    curl -OLsS https://download.jetbrains.com/upsource/upsource-${VERSION}.zip.sha256 && \
    grep "upsource-${VERSION}.zip\$" upsource-${VERSION}.zip.sha256 | sha256sum -c - && \
    unzip upsource-${VERSION}.zip && \
    rm upsource-${VERSION}.zip && \
    rm upsource-${VERSION}.zip.sha256 && \
    mv upsource-* /upsource && \
    apk del .trash && \
    chmod +x /startup.sh && \
    chmod +x /healthcheck.sh

VOLUME ["/upsource/data", "/upsource/backups", "/upsource/logs", "/upsource/conf"]
HEALTHCHECK --interval=2m --timeout=5s CMD /healthcheck.sh
CMD ["/startup.sh"]
